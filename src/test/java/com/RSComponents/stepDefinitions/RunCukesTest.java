package com.RSComponents.stepDefinitions;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
				plugin={"pretty",
					  	"html:target/cucumber-pretty",
						"json:target/cucumber.json"},
				features={"src/test/resources/ProductCategories.feature"},
				glue={"com/RSComponents/stepDefinitions"},
				tags={}
				)
public class RunCukesTest {
}
