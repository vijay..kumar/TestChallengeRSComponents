package com.RSComponents.stepDefinitions;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.RSComponents.coreFramework.AbstractTestCase;
import com.RSComponents.pageObjects.CategoryPage;
import com.RSComponents.pageObjects.LandingPage;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java8.En;

public class CategoryStepDefs extends AbstractTestCase implements En{
	
	@After
    public void cleanUp(Scenario scenario){

        if(scenario.isFailed()){
        	System.err.println("Scenario did not match to the Expected, check the Screenshot: ");

            final byte[] screenShot=((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            scenario.embed(screenShot,"image/png");
        }else{
        	System.out.println("Scenario Executed Successfully: ");
        	categoryPage.stopBrowser();
        }
	}
	
	private LandingPage landingPage;
	private CategoryPage categoryPage;
	
	public CategoryStepDefs(){
		
		
		Given("^User is on LandingPage$", () -> {
		    landingPage = goToProductLaunchPad();
		});
		

		When("^User navigates to LandingPage$", () -> {
		   System.out.println("User Navigates to LandingPage");
		});
		
		
		Then("^User should see all the Menu links are displayed$", (DataTable menuLinks) -> {
			Assert.assertTrue(landingPage.IsProductsLinkDisplayed().contains(menuLinks.asList(String.class).get(1)));
			Assert.assertTrue(landingPage.IsBrandsLinkDisplayed().contains(menuLinks.asList(String.class).get(2)));
			Assert.assertTrue(landingPage.IsNewProductsLinkDisplayed().contains(menuLinks.asList(String.class).get(3)));
			Assert.assertTrue(landingPage.IsMyAccountLinkDisplayed().contains(menuLinks.asList(String.class).get(4)));
			Assert.assertTrue(landingPage.IsServicesLinkDisplayed().contains(menuLinks.asList(String.class).get(5)));
		});

		
		Then("^User should see the few-selected list of categories in the panel$", (DataTable categoriesPanel) -> {
			List<List<String>> categoriesList_FeatureData = categoriesPanel.raw();
			for (int i=0; i<categoriesList_FeatureData.size(); i++)
			{
			System.out.println(categoriesList_FeatureData.get(i).toString());
			}
			Assert.assertEquals(landingPage.getCategoryBattery(), 
									categoriesPanel.asList(String.class).get(1));
			Assert.assertEquals(landingPage.getCategoryCablesAndWires(), 
									categoriesPanel.asList(String.class).get(2));
			Assert.assertEquals(landingPage.getCategoryConnectors(), 
									categoriesPanel.asList(String.class).get(3));
			Assert.assertEquals(landingPage.getCategoryLighting(), 
									categoriesPanel.asList(String.class).get(4));
		});
		
		
		And("^User should be able to navigate to any of the product categories$", () -> {
		    categoryPage = landingPage.clickBatteries();
		});
		

		When("^User clicks on Batteries Category link$", () -> {
		    categoryPage = landingPage.clickBatteries();
		    categoryPage.getCorrectPage();  
		});
		

		Then("^User should see the correct Category page$", (DataTable expectedPage) -> {
		    Assert.assertTrue(categoryPage.getCorrectPage().contains(expectedPage.asList(String.class).get(0)));
		    
		});

		
		When("^User clicks on Cables&Wires Category link$", () -> {
		    categoryPage = landingPage.clickCablesAndWires();
		    categoryPage.getCorrectPage();
		});
		

		Then("^User should see the breadcrumb design of the respective category$", () -> {
			categoryPage.isBreadCrumbViewDisplayed();
			Assert.assertTrue(categoryPage.isBreadCrumbViewDisplayed());
		});
		

		Then("^User should see the respective heading$", (DataTable expectedHeading) -> {
		    categoryPage.ClickOnBreadCrumbCategoryLink();
		    categoryPage.getHeading();
		    Assert.assertTrue(categoryPage.getHeading().contains(expectedHeading.asList(String.class).get(0)));
		});

		When("^User clicks on Lighting Catetory link$", () -> {
		    categoryPage = landingPage.clickLighting();
		   
		});

		Then("^User should see similar 'PopularCategories'$", (DataTable PopularCategories) -> {
		   Assert.assertTrue(categoryPage.getPopularCategories(PopularCategories.asList(String.class).get(1)));
		   Assert.assertTrue(categoryPage.isPopularCategoryListDisplayed());
		   
		});

		When("^User navigates to Connectors Category$", () -> {
		    categoryPage = landingPage.clickConnectors();
		   
		});

		Then("^User should be able to see the Search field on the respective category page$", () -> {
		   Assert.assertTrue(categoryPage.isSearchFormEnabled());
		    
		});
		
	}
}
