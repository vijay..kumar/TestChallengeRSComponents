package com.RSComponents.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.RSComponents.configuration.Utils;
import com.RSComponents.coreFramework.AbstractWebPage;

public class CategoryPage extends AbstractWebPage {

	@FindBy(how = How.CSS, using = "#breadcrumb")
	private WebElement breadCrumb;
	
	@FindBy(how = How.CSS, using = "#breadcrumb ul li:nth-child(3) a")
	private WebElement breadCrumbCategoryLink;
	
	@FindBy(how = How.CSS, using = "h1")
	private WebElement categoryHeading;
	
	@FindBy(how = How.CSS, using = "h4")
	private WebElement popularCategoriesNewUK;
	
	@FindBy(how = How.CSS, using = "#galleryPopularCategory")
	private WebElement popularCategoriesGallery;
	
	@FindBy(how = How.XPATH, using = "//input[@id='searchForm:searchTerm']")
	private WebElement searchField;
	
	
	public CategoryPage(WebDriver driver) {
		super(driver);
	}
	
	public String getCorrectPage(){
		System.out.println(driver.getTitle().toString());
		return driver.getTitle().toString();
	}
	
	public Boolean isBreadCrumbViewDisplayed(){
		Utils.waitForElementVisible(driver, breadCrumb);
		return breadCrumb.isDisplayed();
	}
	
	public void ClickOnBreadCrumbCategoryLink(){
		breadCrumbCategoryLink.click();
	}
	
	public String getHeading(){
		System.out.println(categoryHeading.getText());
		return categoryHeading.getText();
	}
	
	public boolean getPopularCategories(String popularCategories){
		System.out.println(popularCategoriesNewUK.getText().contains(popularCategories));
		return popularCategoriesNewUK.getText().contains(popularCategories);
	}
	
	public Boolean isPopularCategoryListDisplayed(){
		Utils.waitForElementVisible(driver, popularCategoriesGallery);
		return popularCategoriesGallery.isDisplayed();
	}
	
	public Boolean isSearchFormEnabled(){
		Utils.waitForElementVisible(driver, searchField);
		return searchField.isEnabled();
	}
	
	public void stopBrowser(){
        if(driver!=null){
            driver.close();
            driver.quit();
        }
	}

}
