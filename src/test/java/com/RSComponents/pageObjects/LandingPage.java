package com.RSComponents.pageObjects;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.RSComponents.coreFramework.AbstractWebPage;
import com.RSComponents.configuration.Utils;

public class LandingPage extends AbstractWebPage {
	
	@FindBy(how=How.LINK_TEXT, using = "All Products")
	private WebElement allProductsMenuLink;
	    
	@FindBy(how=How.LINK_TEXT, using = "Our Brands")
	private WebElement brandsMenuLink;
	    
	@FindBy(how=How.LINK_TEXT, using = "New Products")
	private WebElement newProductsMenuLink;
	    
	@FindBy(how=How.LINK_TEXT, using = "My Account")
	private WebElement myAccountMenuLink;
	    
	@FindBy(how=How.LINK_TEXT, using = "Our Services")
	private WebElement ourServicesMenuLink;

	@FindBy(how = How.CSS, using = ".//*[@class='list-group-item'][contains(title,'')]")
	private List<WebElement> categoryTitle;
	
	@FindBy(how = How.CSS, using = ".list-group-item[title='Batteries']")
	private WebElement batteries;
	
	@FindBy(how = How.CSS, using = ".list-group-item[title='Cables & Wires']")
	private WebElement cablesAndWires;
	
	@FindBy(how = How.CSS, using = ".list-group-item[title='Connectors']")
	private WebElement connectors;
	
	@FindBy(how = How.CSS, using = ".list-group-item[title='Lighting']")
	private WebElement lighting;
	
	
	public LandingPage(WebDriver driver) {
		super(driver);
	}
	
	public String IsProductsLinkDisplayed(){
		Utils.waitForElementVisible(driver, allProductsMenuLink);
	    System.out.println(allProductsMenuLink.getText());
	    return allProductsMenuLink.getText();
	}
	    
	public String IsBrandsLinkDisplayed(){
	    Utils.waitForElementVisible(driver, brandsMenuLink);
	    System.out.println(brandsMenuLink.getText());
	    return brandsMenuLink.getText();
	 }
	    
	public String IsNewProductsLinkDisplayed(){
	    Utils.waitForElementVisible(driver, newProductsMenuLink);
	    System.out.println(newProductsMenuLink.getText());
	    return newProductsMenuLink.getText();
	}
	    
	public String IsMyAccountLinkDisplayed(){
	    Utils.waitForElementVisible(driver, myAccountMenuLink);
	    System.out.println(myAccountMenuLink.getText());
	    return myAccountMenuLink.getText();
	}
	    
	public String IsServicesLinkDisplayed(){
	    Utils.waitForElementVisible(driver, ourServicesMenuLink);
	    System.out.println(ourServicesMenuLink.getText());
	    return ourServicesMenuLink.getText();
	}
	
	public String getCategoriesList(){
		for(int i=0; i<categoryTitle.size(); i++){
			System.out.println(categoryTitle.get(i).getText().toString());
		}
		return categoryTitle.iterator().next().getText();
	}
	
	public String getCategoryBattery(){
		System.out.println(batteries.getText());
		return batteries.getText();
	}
	public String getCategoryCablesAndWires(){
		System.out.println(cablesAndWires.getText());
		return cablesAndWires.getText();
	}
	public String getCategoryConnectors(){
		System.out.println(connectors.getText());
		return connectors.getText();
	}
	public String getCategoryLighting(){
		System.out.println(lighting.getText());
		return lighting.getText();
	}
	
	
	public CategoryPage clickBatteries(){
		batteries.click();
		return new CategoryPage(driver);	
	}
	
	public CategoryPage clickCablesAndWires(){
		cablesAndWires.click();
		return new CategoryPage(driver);	
	}
	
	public CategoryPage clickConnectors(){
		connectors.click();
		return new CategoryPage(driver);	
	}
	
	public CategoryPage clickLighting(){
		lighting.click();
		return new CategoryPage(driver);	
	}
}
