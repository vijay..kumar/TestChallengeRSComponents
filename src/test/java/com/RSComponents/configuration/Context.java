package com.RSComponents.configuration;

import org.springframework.boot.env.YamlPropertySourceLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.client.RestTemplate;

import com.RSComponents.coreFramework.WebDriverFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class Context {
	
	//for loading Yaml properties
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer(){
		System.out.println("PropertySourcesPlaceholderConfigurer()");
		return new PropertySourcesPlaceholderConfigurer();
	}
		
	@Bean
	public static YamlPropertySourceLoader yamlPropertySourceLoader(){
		System.out.println("YamlPropertySourceLoader()");
		return new YamlPropertySourceLoader();
	}
		
	@Bean
	public ObjectMapper objectMapper(){
		System.out.println("ObjectMapper()");
		return new ObjectMapper();
	}
		
	@Bean
	public RestTemplate restTemplate(){
		System.out.println("RestTemplate()");
		return new RestTemplate();
	}
	
	@Bean
	public static WebDriverFactory webDriverFactory(){
		System.out.println("WebDriverFactory()");
		return new WebDriverFactory();
	}
}
