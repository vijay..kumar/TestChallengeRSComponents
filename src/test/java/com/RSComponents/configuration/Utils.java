package com.RSComponents.configuration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.RSComponents.coreFramework.AbstractWebPage;

public class Utils extends AbstractWebPage {
	
	public Utils(WebDriver driver) {
		super(driver);
	}

	public static final int TIMEOUT=30;
	
	//Wait for element visible
    public static void waitForElementVisible(WebDriver driver, WebElement element){
        WebDriverWait wait=new WebDriverWait(driver,TIMEOUT);
        wait.until(ExpectedConditions.visibilityOf(element));
    }

    //Wait for Element Clickable
    public static void waitForElementClickable(WebDriver driver, WebElement element){
        WebDriverWait wait=new WebDriverWait(driver,TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

}
