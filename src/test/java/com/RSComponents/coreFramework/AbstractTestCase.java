package com.RSComponents.coreFramework;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.ConfigFileApplicationContextInitializer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;

import com.RSComponents.configuration.Context;
import com.RSComponents.pageObjects.LandingPage;

@ContextConfiguration(classes = Context.class, initializers=ConfigFileApplicationContextInitializer.class)
public class AbstractTestCase {

	protected WebDriver driver;
	
	@Value("${rsComponentsUrl}")
	private String rsComponentsUrl;
	
	public void printUrl(){
		System.out.println(rsComponentsUrl);
	}
	
	
	@Autowired
	protected RestTemplate restTemplate;
	
	public void printRestTemplate(){
		System.out.println(restTemplate);
	}
	
	
	@Autowired
	private WebDriverFactory webDriverFactory;
	
	public void printWebDriverFactory(){
		System.out.println(webDriverFactory);
	}
	
	
	public String navigateToRsComponentsUrl(){
		return rsComponentsUrl;
	}
	
	
	
	protected LandingPage goToProductLaunchPad(){
		
		driver = webDriverFactory.initializeWebDriver();
		driver.get(navigateToRsComponentsUrl());
		System.out.println(driver.getTitle());
		
	return new LandingPage(driver);
		
	}
}
