#Author: vkumar.plr@gmail.com
Feature: Test 5 Scenarios for the category page on RS Components website.

  Background: 
    Given User is on LandingPage

  Scenario: Verify header menus on landing page and the list of Categories in the categories-panel as expected
    When User navigates to LandingPage
    Then User should see all the Menu links are displayed
      | Menu         |
      | All Products |
      | Our Brands   |
      | New Products |
      | My Account   |
      | Our Services |
    Then User should see the few-selected list of categories in the panel
      | Categories     |
      | Batteries      |
      | Cables & Wires |
      | Connectors     |
      | Lighting       |
    And User should be able to navigate to any of the product categories

  Scenario: Verify Categories links are clickable and navigates to corresponding category page
    When User clicks on Batteries Category link
    Then User should see the correct Category page
      | Batteries |

  Scenario: Verify clicking on any categories link, should display breadcrumb design and is clickable.
    When User clicks on Cables&Wires Category link
    Then User should see the breadcrumb design of the respective category
    And User should see the respective heading
      | Cables & Wires |

  Scenario: Verify clicking on any category link, should display the corresponding/similar popular categories section.
    When User clicks on Lighting Catetory link
    Then User should see similar 'PopularCategories'
      | PopularCategories               |
      | Popular categories for 'new_uk' |

  Scenario: Verify product-search field is visible on every category link.
    When User navigates to Connectors Category
    Then User should be able to see the Search field on the respective category page
